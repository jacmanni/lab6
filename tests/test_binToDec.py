from binaryConverter import binToDecList

def test_bin_to_dec():
    # Test binary to decimal: [1, 1, 0, 0, 1] converts to 25
    assert binToDecList([1, 1, 0, 0, 1]) == 25
