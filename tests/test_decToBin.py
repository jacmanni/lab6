from binaryConverter import decToBinList

def test_dec_to_bin():
    # Test normal case: 25 converts to [1, 1, 0, 0, 1]
    assert decToBinList(25) == [1, 1, 0, 0, 1]

def test_power_of_two():
    # Test power of two: 8 converts to [1, 0, 0, 0]
    assert decToBinList(8) == [1, 0, 0, 0]

def test_zero():
    # Test zero case: 0 converts to [0]
    assert decToBinList(0) == [0]